#use wml::debian::translation-check translation="6bde107f0f532a81d16326f47a68480ab5eb7957" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes de sécurité ont été découverts dans le navigateur
web Firefox de Mozilla qui pourraient éventuellement avoir pour conséquence
l'exécution de code arbitraire, des problèmes de script intersite et
d'usurpation, la divulgation d'informations, un déni de service ou une
contrefaçon de requête intersite.</p>

<p>Pour la distribution oldstable (Stretch), ces problèmes ont été corrigés
dans la version 60.8.0esr-1~deb9u1.</p>

<p>Pour la distribution stable (Buster), ces problèmes ont été corrigés
dans la version 60.8.0esr-1~deb10u1.</p>

<p><a href="https://security-tracker.debian.org/tracker/CVE-2019-11719">\
CVE-2019-11719</a> et
<a href="https://security-tracker.debian.org/tracker/CVE-2019-11729">\
CVE-2019-11729</a> sont seulement traités dans Stretch, Firefox dans Buster
utilise pour tout le système la copie de NSS qui sera mis à jour
séparément.</p>

<p>Nous vous recommandons de mettre à jour vos paquets firefox-esr.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de firefox-esr,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/firefox-esr">\
https://security-tracker.debian.org/tracker/firefox-esr</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4479.data"
# $Id: $
