#use wml::debian::template title="Événements importants liés à Debian" BARETITLE=true
#use wml::debian::translation-check translation="7ff6f5b6db32d0aa1bc33e38c8014c6a83685f42" maintainer="Jean-Pierre Giraud"

# Translators:
# Martin Quinson
# Frédéric Bothamy, 2007
# Thomas Vincent, 2013
# Baptiste Jammet, 2014

<p>La conférence annuelle de Debian, appelée <a href="https://www.debconf.org/">DebConf</a>
est l'événement majeur du projet Debian. Il y a aussi plusieurs <a
href="https://wiki.debian.org/MiniDebConf">MiniDebConf</a> qui sont des
réunions locales organisées par des membres du projet Debian.
</p>

<p>Les pages liées à la <a href="https://wiki.debian.org/DebianEvents">page
des événements du wiki de Debian</a> fournissent la liste des événements auxquels
Debian est, a été ou sera partie prenante.</p>

<p>Si vous désirez aider Debian à participer à l'un des événements de la
liste ci-dessous, merci de contacter directement la personne indiquée
comme coordinateur Debian de cet événement particulier.</p>

<p>Si vous souhaitez inviter Debian à un nouvel événement, merci d'envoyer
un courriel en anglais à la <a href="eventsmailinglists">liste de diffusion
géographique correspondante</a>.</p>

<p>Si vous voulez aider à prendre en charge la présence de Debian à un événement,
veuillez vérifier sur la page la plus appropriée de <a href="https://wiki.debian.org/DebianEvents">la
section des événements du wiki Debian</a> s'il y a déjà une entrée
correspondant à cet événement. Si oui, inscrivez-vous à cet endroit. Sinon, vous
devriez suivre les <a href="checklist">points à vérifier avant un stand</a>.</p>

<p>Si vous souhaitez voir quels produits sont proposés par d'autres sociétés
avec le logo Debian, et qui peuvent, entre autres choses, être utilisés lors
d'événements, veuillez consulter la page des <a href="merchandise">produits Debian</a>.</p>

<p>Si vous projetez de réaliser une présentation de Debian, vous pouvez créer
une entrée dans la <a href="https://wiki.debian.org/DebianEvents">page des
événements du wiki de Debian</a>. Ensuite, vous pourrez ajouter votre
présentation à la <a href="https://wiki.debian.org/Presentations">collection
des présentations de Debian</a>.</p>

## This probably should be cleaned up, too.
<define-tag event_year>Événements en %d</define-tag>
<define-tag past_words>Événements passés</define-tag>
<define-tag none_word>aucun</define-tag>

#include "$(ENGLISHDIR)/events/index.include"

##<h3>Événements à venir impliquant Debian</h3>

<:= get_future_event_list(); :>

##<p>La liste ci-dessus est généralement incomplète et a été dépréciée en faveur
##de <a href="https://wiki.debian.org/DebianEvents">la page des événements du
##wiki Debian</a>.</p>

<h3>Pages utiles</h3>

<ul>
  <li> <a href="checklist">Points à vérifier avant un stand</a></li>
  <li> <a href="https://wiki.debian.org/DebianEvents">Page des événements du
    wiki Debian</a></li>
  <li> <a href="material">Matériel et marchandises pour un stand</a></li>
  <li> <a href="keysigning">Signature de clés</a></li>
  <li> <a href="https://wiki.debian.org/Presentations">Présentations de
    Debian</a></li>
  <li> <a href="admin">Impliquer</a> events@debian.org</li>
</ul>

<h3>Événements passés</h3>

<p>Les événements anciens qui ont eu lieu avant le basculement vers le <a
href="https://wiki.debian.org/DebianEvents">wiki</a> sont visibles depuis ces
pages :

<:= get_past_event_list(); :>
