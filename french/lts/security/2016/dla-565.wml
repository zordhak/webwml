#use wml::debian::translation-check translation="ce41d997301872adfc27a79ea546429856226b67" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans l'implémentation du
langage de programmation Perl. Le projet « Common Vulnerabilities and
Exposures » (CVE) identifie les problèmes suivants.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-1238">CVE-2016-1238</a>

<p>John Lightsey et Todd Rinaldo ont signalé que le chargement opportuniste
de modules optionnels peut provoquer le chargement involontaire de code par
de nombreux programmes à partir du répertoire de travail courant (qui
pourrait être changé pour un autre répertoire sans que l'utilisateur s'en
rende compte) et éventuellement mener à une élévation de privilèges, comme
cela a été démontré dans Debian avec certaines combinaisons de paquets
installés.</p>

<p>Le problème est lié au chargement de modules par Perl à partir du
tableau de répertoires « includes » (@INC) dans lequel le dernier élément
est le répertoire courant (« . »). Cela signifie que, quand <q>perl</q>
souhaite charger un module (lors d'une première compilation ou du
chargement différé d'un module durant l'exécution), Perl cherche finalement
le module dans le répertoire courant, dans la mesure où « . » est le
dernier répertoire inclus dans son tableau de répertoires inclus à
explorer. Le problème vient de la demande de bibliothèques qui sont dans
« . » mais qui ne sont pas autrement installées.</p>

<p>Avec cette mise à jour, plusieurs modules qui sont connus pour être
vulnérables sont mis à jour pour ne pas charger de modules à partir du
répertoire courant.</p>

<p>En complément, la mise à jour permet le retrait configurable de « . » de
@INC dans /etc/perl/sitecustomize.pl pour une période de transition. Il est
recommandé d'activer ce réglage si la casse potentielle d'un site
particulier a été supputée. Les problèmes dans les paquets fournis dans 
Debian résultant du passage au retrait de « . » dans @INC doivent être
signalés aux mainteneurs de Perl à l'adresse perl@packages.debian.org.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6185">CVE-2016-6185</a>

<p>XSLoader, un module essentiel de Perl pour le chargement dynamique de
bibliothèques C dans le code de Perl, pourrait charger une bibliothèque
partagée à partir d'un emplacement incorrect. XSLoader utilise les
informations de caller() pour localiser le fichier .so à charger. Cela peut
être incorrect si XSLoader::load() est appelé dans une évaluation de chaîne.
Un attaquant peut tirer avantage de ce défaut pour exécuter du code
arbitraire.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 5.14.2-21+deb7u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets perl.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-565.data"
# $Id: $
