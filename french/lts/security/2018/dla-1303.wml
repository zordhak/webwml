#use wml::debian::translation-check translation="ec63203ae7c88b70ad002f6a6d89bd22297d4fb8" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs fonctions étaient extrêmement lentes pour évaluer certaines entrées
à cause de vulnérabilités de retour arrière désastreux dans plusieurs
expressions rationnelles.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-7536">CVE-2018-7536</a>

<p>La fonction django.utils.html.urlize() étaient extrêmement lentes pour
évaluer certaines entrées à cause de vulnérabilités de retour arrière
désastreux dans deux expressions rationnelles. La fonction urlize() est
utilisée pour mettre en œuvre les filtres de modèle urlize and urlizetrunc, qui
donc étaient vulnérables.</p>

<p>Les expressions rationnelles problématiques sont remplacées par une logique
d’analyse qui se comporte de manière similaire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-7537">CVE-2018-7537</a>

<p>Si les méthodes chars() et words() de django.utils.text.Truncator étaient
passées avec l’argument html=True, elles étaient extrêmement lentes à évaluer
certaines entrées à cause d’une vulnérabilité de retour sur trace désastreux
dans une expression rationnelle. Les méthodes chars() et words() sont utilisées
pour mettre en œuvre les filtres de modèle truncatechars_html et
truncatewords_html, qui donc étaient vulnérables.</p>

<p>Le problème de retour sur trace dans les expressions rationnelles est
corrigé.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 1.4.22-1+deb7u4.</p>


<p>Nous vous recommandons de mettre à jour vos paquets python-django.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1303.data"
# $Id: $
