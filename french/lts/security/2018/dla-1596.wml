#use wml::debian::translation-check translation="1d1c1ba842e225bf68a6fed5744786cc779234f7" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il pourrait exister une vulnérabilité de déni de service (DoS) dans squid3,
due à une fuite de mémoire dans le code de rejet de requête SNMP lorsque SNMP
est activé. Dans des environnements où des restrictions de mémoire par processus
ne sont pas appliquées scrupuleusement, un attaquant distant peut utiliser toute
la mémoire disponible au processus Squid, causant son plantage.</p>

<p>Pour Debian 8 <q>Jessie</q>, ce problème a été corrigé dans la version 3.4.8-6+deb8u6.</p>

<p>Nous vous recommandons de mettre à jour vos paquets squid3.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1596.data"
# $Id: $
