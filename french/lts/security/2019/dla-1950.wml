#use wml::debian::translation-check translation="0e295902fac5fb4c33646ecaaeb2e54a8d6ac718" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Une vulnérabilité de dépassement de tampon basé sur le tas a été découverte
dans openjpeg2, le codec JPEG 2000 au code source ouvert. Cette vulnérabilité
est causée par une validation insuffisante de largeur et hauteur des composantes
d’image dans color_apply_icc_profile (src/bin/common/color.c).</p>

<p>Des attaquants distants peuvent exploiter cette vulnérabilité à l'aide d'un
fichier JP2 contrefait, conduisant à un déni de service (plantage d'application)
ou n’importe quel autre comportement non défini.</p>

<p>Pour Debian 8 <q>Jessie</q>, ce problème a été corrigé dans
la version 2.1.0-2+deb8u8.</p>
<p>Nous vous recommandons de mettre à jour vos paquets openjpeg2.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1950.data"
# $Id: $
