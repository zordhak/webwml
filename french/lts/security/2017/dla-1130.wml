#use wml::debian::translation-check translation="72f9357c5679b1e36a67da1a7afcf729e69b36c2" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Cette publication corrige un certain nombre de problèmes de sécurité dans
graphicsmagick.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14103">CVE-2017-14103</a>

<p>Les fonctions ReadJNGImage et ReadOneJNGImage dans coders/png.c ne géraient pas
correctement les pointeurs d’image après certaines conditions d’erreur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14314">CVE-2017-14314</a>

<p>Lecture hors limites de tampon basé sur le tas dans DrawDashPolygon().</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14504">CVE-2017-14504</a>

<p>Déréférencement de pointeur NULL déclenché par un fichier mal formé.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14733">CVE-2017-14733</a>

<p>Garantie de détection des images alpha avec trop peu de couleurs.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14994">CVE-2017-14994</a>

<p>DCM_ReadNonNativeImages() peut produire une liste d’images sans image fixe,
aboutissant à un pointeur NULL d’image.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14997">CVE-2017-14997</a>

<p>Dépassement de capacité par le bas non signé conduisant à une requête
d’allocation étonnamment grande.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 1.3.16-1.1+deb7u10.</p>

<p>Nous vous recommandons de mettre à jour vos paquets graphicsmagick.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1130.data"
# $Id: $
