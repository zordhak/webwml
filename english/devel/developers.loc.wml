#use wml::debian::template title="Developer Locations"

<p>Many people have expressed interest in information on the
location of Debian developers.
We therefore decided to add, as part of the developer database
a field where developers can specify their world coordinates.

<p>The map below was generated from an anonymized
<a href="developers.coords">list of developer coordinates</a>
using the program
<a href="https://packages.debian.org/stable/graphics/xplanet">
xplanet</a>.

<p><img src="developers.map.jpeg" alt="World Map">

<p>If you are a developer and would like to add your coordinates
to your database entry, log in to the
<a href="https://db.debian.org">Debian Developers' Database</a>
and modify your entry. If you don't know the coordinates of your
hometown, You should be able to find it from one of the following
locations:
<ul>
<li><a href="https://osm.org">Openstreetmap</a>
You can search for your city in the search bar.
Select the direction arrows next to the search bar. Then, drag the
green marker to the OSM map. The coords will appear in the 'from' box.
</ul>

<p>The format for coordinates is one of the following:
<dl>
<dt>Decimal Degrees
<dd>The format is +-DDD.DDDDDDDDDDDDDDD.  This  is  the
    format programs like xearth use and the format that
    many positioning web sites use.  However  typically
    the precision is limited to 4 or 5 decimals.
<dt>Degrees Minutes (DGM)
<dd>The  format  is +-DDDMM.MMMMMMMMMMMMM. It is not an
    arithmetic type, but a packed representation of two
    separate units, degrees and minutes. This output is
    common from some types of hand held GPS  units  and
    from NMEA format GPS messages.
<dt>Degrees Minutes Seconds (DGMS)
<dd>The  format  is +-DDDMMSS.SSSSSSSSSSS. Like DGM, it
    is not an arithmetic type but a packed  representation
    of  three separate units: degrees, minutes and
    seconds. This output is typically derived from  web
    sites  that  give 3  values for each position. For
    instance 34:50:12.24523 North might be the position
    given, in DGMS it would be +0345012.24523.
</dl>

<p>
For  Latitude  + is North, for Longitude + is East. It is
important to specify enough leading zeros to dis-ambiguate
the  format  that  is  being used if your position is less
than 2 degrees from a zero point.
