<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several issues have been found in exiv2, a EXIF/IPTC/XMP metadata
manipulation tool.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-17581">CVE-2018-17581</a>

     <p>A stack overflow due to a recursive function call causing excessive
     stack consumption which leads to denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19107">CVE-2018-19107</a>

     <p>A heap based buffer over-read caused by an integer overflow could
     result in a denial of service via a crafted file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19108">CVE-2018-19108</a>

     <p>There seems to be an infinite loop inside a function that can be
     activated by a crafted image.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19535">CVE-2018-19535</a>

     <p>A heap based buffer over-read caused could result in a denial of
     service via a crafted file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20097">CVE-2018-20097</a>

     <p>A crafted image could result in a denial of service.</p></li>

</ul>

<p>For Debian 8 &quot;Jessie&quot;, these problems have been fixed in version
0.24-4.1+deb8u3.</p>

<p>We recommend that you upgrade your exiv2 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1691.data"
# $Id: $
