<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A heap buffer overflow vulnerability was discovered in openjpeg2, the
open-source JPEG 2000 codec. This vulnerability is caused by insufficient
validation of width and height of image components in color_apply_icc_profile
(src/bin/common/color.c).</p>
<p>Remote attackers might leverage this vulnerability
via a crafted JP2 file, leading to denial of service (application crash) or any
other undefined behavior.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
2.1.0-2+deb8u8.</p>

<p>We recommend that you upgrade your openjpeg2 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1950.data"
# $Id: $
