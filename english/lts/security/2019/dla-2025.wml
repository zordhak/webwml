<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>The OpenSLP package had two open security issues, as described.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-17833">CVE-2017-17833</a>

    <p>OpenSLP releases in the 1.0.2 and 1.1.0 code streams have a heap-related
    memory corruption issue which may manifest itself as a denial-of-service
    or a remote code-execution vulnerability.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5544">CVE-2019-5544</a>

    <p>OpenSLP as used in ESXi and the Horizon DaaS appliances has a heap
    overwrite issue. VMware has evaluated the severity of this issue to be
    in the critical severity range.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.2.1-10+deb8u2.</p>

<p>We recommend that you upgrade your openslp-dfsg packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-2025.data"
# $Id: $
