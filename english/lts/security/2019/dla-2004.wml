<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>A flaw was found in the <q>deref</q> plugin of 389-ds-base where it could
use the <q>search</q> permission to display attribute values.</p>

<p>In some configurations, this could allow an authenticated attacker
to view private attributes, such as password hashes.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1.3.3.5-4+deb8u7.</p>

<p>We recommend that you upgrade your 389-ds-base packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-2004.data"
# $Id: $
