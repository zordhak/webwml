<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>A vulnerability was found in the Bind DNS Server. Limits on
simultaneous tcp connections have not been enforced correctly and could
lead to exhaustion of file descriptors. In the worst case this could
affect the file descriptors of the whole system.</p>


<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1:9.9.5.dfsg-9+deb8u18.</p>

<p>We recommend that you upgrade your bind9 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1859.data"
# $Id: $
