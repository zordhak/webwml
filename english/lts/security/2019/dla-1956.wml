<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>ruby-openid performed discovery first, and then verification. This allowed an
attacker to change the URL used for discovery and trick the server into
connecting to the URL. This server in turn could be a private server not
publicly accessible.</p>

<p>Furthermore, if the client that uses this library discloses connection errors,
this in turn could disclose information from the private server to the
attacker.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
2.5.0debian-1+deb8u1.</p>

<p>We recommend that you upgrade your ruby-openid packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1956.data"
# $Id: $
