<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several issues were discovered in TIFF, the Tag Image File Format
library, that allowed remote attackers to cause a denial-of-service or
other unspecified impact via a crafted image file.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-11613">CVE-2017-11613</a>

<p>DoS vulnerability:
    A crafted input will lead to a denial of service attack. During the
    TIFFOpen process, td_imagelength is not checked. The value of
    td_imagelength can be directly controlled by an input file. In the
    ChopUpSingleUncompressedStrip function, the _TIFFCheckMalloc
    function is called based on td_imagelength. If the value of
    td_imagelength is set close to the amount of system memory, it will
    hang the system or trigger the OOM killer.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10963">CVE-2018-10963</a>

<p>DoS vulnerability:
    The TIFFWriteDirectorySec() function in tif_dirwrite.c in LibTIFF
    allows remote attackers to cause a denial of service (assertion
    failure and application crash) via a crafted file, a different
    vulnerability than <a href="https://security-tracker.debian.org/tracker/CVE-2017-13726">CVE-2017-13726</a>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5784">CVE-2018-5784</a>

<p>DoS vulnerability: 
    In LibTIFF, there is an uncontrolled resource consumption in the
    TIFFSetDirectory function of tif_dir.c. Remote attackers could
    leverage this vulnerability to cause a denial of service via a
    crafted tif file.
    This occurs because the declared number of directory entries is not
    validated against the actual number of directory entries.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-7456">CVE-2018-7456</a>

<p>NULL Pointer Dereference:
    A NULL Pointer Dereference occurs in the function TIFFPrintDirectory
    in tif_print.c in LibTIFF when using the tiffinfo tool to print
    crafted TIFF information, a different vulnerability than
    <a href="https://security-tracker.debian.org/tracker/CVE-2017-18013">CVE-2017-18013</a>. (This affects an earlier part of the
    TIFFPrintDirectory function that was not addressed by the
    <a href="https://security-tracker.debian.org/tracker/CVE-2017-18013">CVE-2017-18013</a> patch.)</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-8905">CVE-2018-8905</a>

<p>Heap-based buffer overflow:
    In LibTIFF, a heap-based buffer overflow occurs in the function
    LZWDecodeCompat in tif_lzw.c via a crafted TIFF file, as
    demonstrated by tiff2ps.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
4.0.3-12.3+deb8u6.</p>

<p>We recommend that you upgrade your tiff packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1411.data"
# $Id: $
