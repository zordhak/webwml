<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities were found in Spamassassin, which could lead
to Remote Code Execution and Denial of Service attacks under certain
circumstances.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-1238">CVE-2016-1238</a>

    <p>Many Perl programs do not properly remove . (period) characters
    from the end of the includes directory array, which might allow
    local users to gain privileges via a Trojan horse module under the
    current working directory.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-15705">CVE-2017-15705</a>

    <p>A denial of service vulnerability was identified that exists in
    Apache SpamAssassin before 3.4.2. The vulnerability arises with
    certain unclosed tags in emails that cause markup to be handled
    incorrectly leading to scan timeouts. This can cause carefully
    crafted emails that might take more scan time than expected
    leading to a Denial of Service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-11780">CVE-2018-11780</a>

    <p>A potential Remote Code Execution bug exists with the PDFInfo
    plugin in Apache SpamAssassin before 3.4.2.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-11781">CVE-2018-11781</a>

    <p>Apache SpamAssassin 3.4.2 fixes a local user code injection in the
    meta rule syntax.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
3.4.2-0+deb8u1. Upstream strongly advocates upgrading to the latest
upstream version so we are following that recommendation and
backported the version published as part of the 9.6 stretch release,
which also fixes many critical bugs.</p>

<p>We recommend that you upgrade your spamassassin packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1578.data"
# $Id: $
