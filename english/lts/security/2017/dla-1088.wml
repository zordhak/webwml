<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Irssi has some issues where remote attackers might be able to cause a crash.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9468">CVE-2017-9468</a>

	<p>In irssi, when receiving a DCC message without source nick/host, it
	attempts to dereference a NULL pointer.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9469">CVE-2017-9469</a>

	<p>In irssi, when receiving certain incorrectly quoted DCC files, it tries to
	find the terminating quote one byte before the allocated memory.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
0.8.15-5+deb7u2.</p>

<p>We recommend that you upgrade your irssi packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1088.data"
# $Id: $
