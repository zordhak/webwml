<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that there was a denial-of-service vulnerability in
the Strongswan Virtual Private Network (VPN) software.</p>

<p>Specific RSA signatures passed to the gmp plugin for verification could
cause a null-pointer dereference. Potential triggers are signatures in
certificates, but also signatures used during IKE authentication.</p>

<p>For more details, please see:</p>

<p><url "https://www.strongswan.org/blog/2017/08/14/strongswan-vulnerability-(cve-2017-11185).html"></p>

<p>For Debian 7 <q>Wheezy</q>, this issue has been fixed in strongswan version
4.5.2-1.5+deb7u10.</p>

<p>We recommend that you upgrade your strongswan packages.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1059.data"
# $Id: $
