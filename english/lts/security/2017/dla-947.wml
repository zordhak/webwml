<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that icu, the International Components for Unicode
library, did not correctly validate its input. An attacker could use
this problem to trigger an out-of-bound write through a heap-based
buffer overflow, thus causing a denial of service via application
crash, or potential execution of arbitrary code.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
4.8.1.1-12+deb7u7.</p>

<p>We recommend that you upgrade your icu packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-947.data"
# $Id: $
