<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A security vulnerability was discovered in Asterisk, an Open
Source PBX and telephony toolkit, that may lead to unauthorized
command execution.</p>

<p>The app_minivm module has an <q>externnotify</q> program configuration option
that is executed by the MinivmNotify dialplan application. The
application uses the caller-id name and number as part of a built
string passed to the OS shell for interpretation and execution. Since
the caller-id name and number can come from an untrusted source, a
crafted caller-id name or number allows an arbitrary shell command
injection.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1:1.8.13.1~dfsg1-3+deb7u7.</p>

<p>We recommend that you upgrade your asterisk packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1122.data"
# $Id: $
