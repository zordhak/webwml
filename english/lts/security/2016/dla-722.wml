<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>An information disclosure vulnerability was found in irssi.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7553">CVE-2016-7553</a>

  <p>Other users on the same machine as the user running irssi with
  buf.pl loaded may be able to retrieve the whole window contents
  after /UPGRADE.
  Furthermore, this dump of the windows contents is never removed
  afterwards.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, this problems have been fixed in version
0.8.15-5+deb7u1.</p>

<p>We recommend that you upgrade your irssi packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-722.data"
# $Id: $
