<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The crypto library libgcrypt11 has a weakness in the random number
generator.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6313">CVE-2016-6313</a>

  <p>Felix Dörre and Vladimir Klebanov from the Karlsruhe Institute of
  Technology found a bug in the mixing functions of Libgcrypt's random
  number generator. An attacker who obtains 4640 bits from the RNG can
  trivially predict the next 160 bits of output.</p></li>

</ul>

<p>A first analysis on the impact of this bug in GnuPG shows that existing
RSA keys are not weakened. For DSA and Elgamal keys it is also unlikely
that the private key can be predicted from other public information.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.5.0-5+deb7u5.</p>

<p>We recommend that you upgrade your libgcrypt11 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-600.data"
# $Id: $
