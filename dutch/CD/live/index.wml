#use wml::debian::cdimage title="Live installatie-images"
#use wml::debian::release_info
#use wml::debian::installer
#include "$(ENGLISHDIR)/releases/images.data"
#use wml::debian::translation-check translation="566eb2b064b9f0ba2857df0113835d39cd34e5c2"

<p>Een <q>live installatie</q>-image bevat een Debian-systeem dat kan
opstarten zonder een bestand op de harde schijf te wijzigen. Ook maakt de
inhoud van het image de installatie van Debian mogelijk.
</p>

<p><a name="choose_live"><strong>Is een live-image geschikt voor mij?</strong></a> 
Hierna volgen een aantal in aanmerking te nemen zaken die u kunnen helpen een beslissing te nemen.
<ul>
<li><b>Varianten:</b> De live-images bestaan in verschillende "varianten"
die een keuze bieden uit bureaubladomgevingen (GNOME, KDE, LXDE, Xfce,
Cinnamon en MATE). Veel gebruikers zullen de initiële selectie pakketten
geschikt vinden en nadien eventuele bijkomende pakketten welke ze nodig hebben,
via het netwerk installeren.
<li><b>Architectuur:</b> Momenteel worden enkel images voor de twee
populairste architecturen, 32-bits PC (i386) en 64-bits PC (amd64),
aangeboden.
<li><b>Grootte:</b> Elk image is veel kleiner dan de volledige collectie op
DVD-images, maar groter dan het netwerkinstallatiemedium.
<li><b>Taal:</b> De images bevatten niet de volledige verzameling pakketten
voor taalondersteuning. Als u invoermethoden, lettertypes of bijkomende
pakketten nodig heeft voor uw taal, moet u deze nadien installeren.
</ul>

<p>U kunt de volgende live installatie-images downloaden:</p>
 
<ul>

  <li>Officiële <q>live installatie</q>-images voor de <q>stabiele</q> release &mdash; <a
  href="#live-install-stable">zie hieronder</a></li>

</ul>


<h2 id="live-install-stable">Officiële live installatie-images voor de <q>stabiele</q> release</h2>

<p>Aangeboden in verschillende varianten, die, zoals hierboven aangegeven,
onderling in grootte verschillen. Deze images zijn geschikt om een
Debian-systeem met een geselecteerde collectie standaardpakketten uit te
proberen en ze met behulp van diezelfde media te installeren.</p>

<div class="line">
<div class="item col50">
<p><strong>DVD/USB (via <a
href="$(HOME)/CD/torrent-cd">BitTorrent</a>)</strong></p>
<p><q>Hybride</q> ISO imagebestanden die op een DVD-R(W) gebrand kunnen
worden en ook naar een voldoende grote USB-stick gekopieerd kunnen worden.
Indien u dat kunt, gebruik dan BitTorrent, omdat dit de belasting van
onze servers beperkt.</p>
	  <stable-live-install-bt-cd-images />
</div>

<div class="item col50 lastcol"> <p><strong>DVD/USB</strong></p>
<p><q>Hybride</q> ISO imagebestanden die op een DVD-R(W) gebrand kunnen
worden en ook naar een voldoende grote USB-stick gekopieerd kunnen worden.</p>
       <stable-live-install-iso-cd-images /> 
</div> </div>

<p>Raadpleeg de <a href="../faq/">FAQ</a> voor informatie over wat deze
bestanden zijn en hoe u ze kunt gebruiken.</p>

<p>Indien u van plan bent Debian te installeren met het gedownloade live-image,
moet u zeker de <a href="$(HOME)/releases/stable/installmanual">uitgebreide
informatie over het installatieproces</a> bekijken.</p>

<p>Raadpleeg <a href="$(HOME)/devel/debian-live"> de projectpagina van Debian Live</a> voor
extra informatie over de Debian Live-systemen die op deze images aangeboden worden.</p>

