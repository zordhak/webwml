#use wml::debian::template title="Wie gebruikt Debian?" BARETITLE=true
#use wml::debian::toc
#use wml::debian::users_list
#use wml::debian::translation-check translation="257c21bc3adb2b3c7174439a605ea9010623492d"

<p>
  Hieronder is de beschrijving opgenomen van sommige organisaties die
  Debian hebben ingezet en ervoor gekozen hebben om een korte beschrijving
  te geven over hoe zij Debian gebruiken en waarom ze daarvoor gekozen hebben.
  De items werden alfabetisch gerangschikt. Indien u in deze lijst wilt
  opgenomen worden, volg dan <a href="#submissions">deze instructies</a>.
</p>

<toc-display />

<toc-add-entry name="edu">Onderwijsinstellingen</toc-add-entry>
<:= get_users_list ('$(ENGLISHDIR)/users', 'edu', '.*') :>

<toc-add-entry name="com">Commercieel</toc-add-entry>
<:= get_users_list ('$(ENGLISHDIR)/users', 'com', '.*') :>

<toc-add-entry name="org">Non-profit organisaties</toc-add-entry>
<:= get_users_list ('$(ENGLISHDIR)/users', 'org', '.*') :>

<toc-add-entry name="gov">Overheidsorganisaties</toc-add-entry>
<:= get_users_list ('$(ENGLISHDIR)/users', 'gov', '.*') :>

<hr />

<h2><a name="submissions" id="submissions">Inzendingen</a></h2>
<p>
  Om in deze lijst opgenomen te worden, moet u de volgende informatie in een
  e-amail sturen naar <a
  href="mailto:debian-www@lists.debian.org?subject=Who's%20using%20Debian%3F">debian-www@lists.debian.org</a>
  (gelieve als onderwerp <q>Who's using Debian</q> te gebruiken, anders zouden
  we uw inzending misschien niet opmerken).
  De inzendingen moeten in het Engels gedaan worden. Indien u het Engels niet
  beheerst, zend dan uw inzending naar de
  <a href="https://lists.debian.org/i18n.html">passende vertaallijst</a>.
  Merk op dat de e-mail met uw inzending gepost zal worden in een publieke
  mailinglijst.
</p>

<p>
  Stuur enkel informatie in over organisaties die u vertegenwoordigt. Mogelijk
  moet u binnen uw organisatie om toelating vragen vooraleer u hier gegevens
  inzendt. Indien u een consultant bent die minstens een deel van uw inkomen
  verwerft door <strong>betaalde</strong> ondersteuning over Debian te bieden,
  moet u eerder op onze  <a href="$(HOME)/consultants/">Consultants</a>-pagina
  zijn.
</p>

<p>
  Stuur geen informatie in over private individuen. We waarderen uw steun,
  maar hebben onvoldoende mensen ter beschikking om informatie bij te houden
  over de vele individuen die Debian gebruiken.
</p>

<p>
  <strong>Neem enkel informatie op waarvan u het goed vindt dat deze openbaar
  wordt.</strong>
</p>

<ol>
  <li>
    <p>
      Naam van de organisatie (in de vorm van <em>afdeling</em>, <em>organisatie</em>, <em>gemeente/stad</em> (facultatief), <em>regio/provincie</em> (facultatief), <em>land</em>).
      Bijvoorbeeld: AI Lab, Massachusetts Institute of Technology, USA
    </p>
    <p>
      Niet alle items zijn reeds in deze vorm opgemaakt, maar we zouden deze welke nog niet in deze vorm opgemaakt zijn, graag omzetten.
    </p>
  </li>
  <li>Soort organisatie (selecteer <q>onderwijs</q>, <q>non-profit</q>, <q>commercieel</q>, of <q>overheid</q>)</li>
  <li>Link naar de homepagina <em>(facultatief)</em></li>
  <li>Een of twee korte paragrafen waarin u beschrijft hoe uw organisatie
  Debian gebruikt. Tracht gedetailleerd te rapporteren, zoals over het aantal
  werkstations/servers, de software die erop uitgevoerd wordt (de versie ervan
  vermelden is niet nodig) en waarom u Debian verkoos boven de concurrentie.
  </li>
</ol>

<p>
  Indien u een organisatie vertegenwoordigt die overweegt om een Debian
  Partner te worden, raadpleeg dan ons
  <a href="$(HOME)/partners/">Partnerprogramma</a> voor meer informatie over
  hoe u Debian op permanente basis kunt helpen. Raadpleeg voor donaties onze
  <a href="$(HOME)/donations/">Donaties</a>-pagina.
</p>
