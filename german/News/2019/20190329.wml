<define-tag pagetitle>Handshake spendet 300.000 USD an Debian</define-tag>
<define-tag release_date>2019-03-29</define-tag>
#use wml::debian::news
#use wml::debian::translation-check translation="b814e5f8f89e2086e558034fbf26a53d5f985512" maintainer="Erik Pfannenstein"


<p>
Im Jahr 2018 hat das Debian-Projekt eine Spende über 300.000 USD von 
<a href="https://handshake.org/">Handshake</a>, einer Organisation, die ein experimentelles 
Peer-to-Peer-Root-Domain-Naming-System entwickelt, erhalten.
</p>

<p>
Dieser beträchtliche finanzielle Beitrag wird Debian dabei helfen, den 
Hardware-Ersetzungsplan der 
<a href="https://wiki.debian.org/Teams/DSA">Debian System-Administratoren</a>
weiterzuverfolgen. Nach diesem Plan werden Server und andere Hardware-Komponenten erneuert, 
um die Zuverlässigkeit der Entwicklungs- und Gemeinschaftsinfrastruktur des Projekts zu 
verbessern.
</p>

<p>
Debians Projektleiter Chris Lamb hat der Handshake Foundation seinen <q><em>tief 
empfundenen und aufrichtigen Dank für ihre Unterstützung des Debian-Projekts</em></q> 
ausgesprochen. <q><em>Beiträge wie dieser machen es für eine große Anzahl unterschiedlicher 
Menschen aus aller Welt möglich, zusammen an unserem gemeinsamen Ziel eines vollkommen 
freien <q>universellen</q> Betriebssystems zu arbeiten.</em></q>
</p>

<p>
Handshake ist ein dezentrales, berechtigungsfreies Naming-Protokoll, das mit DNS kompatibel 
ist und bei dem jeder Teilnehmer (Peer) die Root-Zone validiert und verwaltet. Ziel von 
Handshake ist es, eine Alternative zu den bestehenden Zertifikats-Autoritäten aufzubauen.
</p>

<p>
Das Handshake-Projekt, seine Sponsoren und Unterstützer erkennen freie und quelloffene 
Software als unentbehrlichen Teil des Fundaments des Internets und des Projekts selbst an, 
weswegen sie beschlossen haben, 10.200.000 USD in verschiedene FLOSS-Entwickler und 
Projekte sowie gemeinnützige Organisationen und Universitäten, welche die Entwicklung freier 
Software unterstützen, zu investieren.</p>

<p>
Handshake, vielen Dank für eure Förderung!
</p>


<h2>Über Debian</h2>

<p>Das Debian-Projekt ist ein Zusammenschluss von Entwicklern Freier Software, 
die ihre Zeit und Bemühungen einbringen, um das vollständig freie Betriebssystem 
Debian zu erschaffen.</p>

<h2>Kontaktinformationen</h2>

<p>Für weitere Informationen besuchen Sie bitte die Debian-Webseiten unter
<a href="$(HOME)/">https://www.debian.org/</a> oder schicken eine E-Mail (auf 
Englisch) an &lt;press@debian.org&gt;.</p>
