#use wml::debian::translation-check translation="f797e8b952b201d953c4e7de221fd635808136e4" maintainer="Lev Lamberov"
<define-tag pagetitle>Обновлённый Debian 9: выпуск 9.9</define-tag>
<define-tag release_date>2019-04-27</define-tag>
#use wml::debian::news

<define-tag release>9</define-tag>
<define-tag codename>stretch</define-tag>
<define-tag revision>9.9</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Проект Debian с радостью сообщает о девятом обновлении своего
стабильного выпуска Debian <release> (кодовое имя <q><codename></q>).
Это обновление в основном содержит исправления проблем безопасности,
а также несколько корректировок серьёзных проблем. Рекомендации по безопасности
опубликованы отдельно и указываются при необходимости.</p>

<p>Заметьте, что это обновление не является новой версией Debian
<release>, а лишь обновлением некоторых включённых в выпуск пакетов. Нет
необходимости выбрасывать старые носители с выпуском <q><codename></q>. После установки
пакеты можно обновить до текущих версий, используя актуальное
зеркало Debian.</p>

<p>Тем, кто часто устанавливает обновления с security.debian.org, не придётся
обновлять много пакетов, большинство обновлений с security.debian.org
включены в данное обновление.</p>

<p>Новые установочные образы будут доступны позже в обычном месте.</p>

<p>Обновление существующих систем до этой редакции можно выполнить с помощью
системы управления пакетами, используя одно из множества HTTP-зеркал Debian.
Исчерпывающий список зеркал доступен на странице:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>

<p>В случае данной редакции тем, кто использует инструмент <q>apt-get</q>
для выполнения обновления, требуется убедиться, что используется команда
<q>dist-upgrade</q>, для того, чтобы было произведено обновление до новых пакетов ядра. Пользователям других
инструментов, таких как <q>apt</q> и <q>aptitude</q>, следует использовать команду <q>upgrade</q>.</p>

<h2>Исправления различных ошибок</h2>

<p>Данное стабильное обновление вносит несколько важных исправлений для следующих пакетов:</p>
<table border=0>
<tr><th>Пакет</th>               <th>Причина</th></tr>
<correction audiofile "Исправление отказа в обслуживании [CVE-2018-13440] и переполнения буфера [CVE-2018-17095]">
<correction base-files "Обновление для текущей редакции">
<correction bwa "Исправление переполнения буфера [CVE-2019-10269]">
<correction ca-certificates-java "Исправление специфичного для bash кода в postinst и jks-keystore">
<correction cernlib "Применение флага оптимизации -O для модулей Fortran вместо -O2, так как последний приводил к созданию сломанного кода; исправление проблемы сборки на arm64 путём отключения PIE для исполняемых файлов Fortran">
<correction choose-mirror "Обновление поставляемого в пакете списка зеркал">
<correction chrony "Исправление журналирования измерений и статистики, а также остановка chronyd на некоторых платформах с включённой фильтрацией seccomp">
<correction ckermit "Отключение проверки версии OpenSSL">
<correction clamav "Исправление обращения за пределами выделенного буфера памяти при сканировании PDF-документов [CVE-2019-1787], PE-файлов, сжатых с помощью Aspack [CVE-2019-1789] и OLE2-файлов [CVE-2019-1788]">
<correction dansguardian "Добавление <q>missingok</q> в настройку logrotate">
<correction debian-installer "Повторная сборка для proposed-updates">
<correction debian-installer-netboot-images "Повторная сборка для proposed-updates">
<correction debian-security-support "Обновление статуса поддержки">
<correction diffoscope "Исправление тестов, чтобы они работали в Ghostscript 9.26">
<correction dns-root-data "Обновление корневых данных до версии 2019031302">
<correction dnsruby "Добавление нового корневого ключа (KSK-2017); в ruby 2.3.0 директива TimeoutError считается устаревшей, использование Timeout::Error">
<correction dpdk "Новый стабильный выпуск основной ветки разработки">
<correction edk2 "Исправление переполнения буфера в службе BlockIo [CVE-2018-12180]; DNS: проверка размера полученного пакета до его использования [CVE-2018-12178]; исправление переполнения стека из-за обработки повреждённого файла в формате BMP [CVE-2018-12181]">
<correction firmware-nonfree "atheros / iwlwifi: обновление прошивки BlueTooth [CVE-2018-5383]">
<correction flatpak "Отклонение всех ioctl, которые интерпретируются ядром как TIOCSTI [CVE-2019-10063]">
<correction geant321 "Повторная сборка с учётом исправления оптимизаций Fortran в cernlib">
<correction gnome-chemistry-utils "Прекращение сборки устаревшего пакета gcu-plugin">
<correction gocode "gocode-auto-complete-el: перенос пакета auto-complete-el в Pre-Depends, чтобы гарантировать успешность обновления">
<correction gpac "Исправление переполнений буфера [CVE-2018-7752 CVE-2018-20762], переполнений динамической памяти [CVE-2018-13005 CVE-2018-13006 CVE-2018-20761], записи за пределами выделенного буфера памяти [CVE-2018-20760 CVE-2018-20763]">
<correction icedtea-web "Прекращение сборки браузерного дополнения, которое более не работает с Firefox 60">
<correction igraph "Исправление аварийной остановки при загрузке специально сформированных Graph-файлов [CVE-2018-20349]">
<correction jabref "Исправление атаки через внешнюю сущность XML [CVE-2018-1000652]">
<correction java-common "Удаление пакета default-java-plugin, поскольку удаляется XUL-дополнение icedtea-web">
<correction jquery "Предотвращение засорения Object.prototype [CVE-2019-11358]">
<correction kauth "Исправление небезопасной обработки аргументов во вспомогательных модулях [CVE-2019-7443]">
<correction libdate-holidays-de-perl "Добавление 8 марта (с 2019 года) и 8 мая (только в 2020 году) в качестве праздников (только для Берлина)">
<correction libdatetime-timezone-perl "Обновление поставляемых в пакете данных">
<correction libreoffice "Добавление следующей эры Рэйва (для Японии); указание для пакета -core конфликта с пакетом openjdk-8-jre-headless (= 8u181-b13-2~deb9u1), содержащем сломанный класс ClassPathURLCheck">
<correction linux "Новая стабильная версия основной версии разработки">
<correction linux-latest "Обновление для -9 версии ABI ядра">
<correction mariadb-10.1 "Новая стабильная версия основной ветки разработки">
<correction mclibs "Повторная сборка с учётом исправления оптимизаций Fortran в cernlib">
<correction ncmpc "Исправление разыменования NULL-указателя [CVE-2018-9240]">
<correction node-superagent "Исправление атак по типу ZIP-бомба [CVE-2017-16129]; исправление синтаксической ошибки">
<correction nvidia-graphics-drivers "Новый стабильный выпуск основной ветки разработки [CVE-2018-6260]">
<correction nvidia-settings "Новый стабильный выпуск основной ветки разработки">
<correction obs-build "Не разрешать запись в файлы основной системы [CVE-2017-14804]">
<correction paw "Повторная сборка с учётом исправления оптимизаций Fortran в cernlib">
<correction perlbrew "Разрешение HTTPS URL CPAN">
<correction postfix "Новый стабильный выпуск основной ветки разработки">
<correction postgresql-9.6 "Новый стабильный выпуск основной ветки разработки">
<correction psk31lx "Исправление версии с целью правильной сортировки, чтобы избежать потенциальных проблем с обновлениями">
<correction publicsuffix "Обновление поставляемых в пакете данных">
<correction pyca "Добавление <q>missingok</q> в настройку logrotate">
<correction python-certbot "Возврат к debhelper compat 9, чтобы гарантировать правильный запуск таймеров systemd">
<correction python-cryptography "Удаление BIO_callback_ctrl: прототип отличается от определения OpenSSL после его изменения (исправления) в OpenSSL">
<correction python-django-casclient "Применение исправления промежуточного ПО django 1.10; python(3)-django-casclient: исправление отсутствующих зависимостей от python(3)-django">
<correction python-mode "Удаление поддержки xemacs21">
<correction python-pip "Корректный перехват HTTPError в запросах в index.py">
<correction python-pykmip "Исправление потенциального отказа в обслуживании [CVE-2018-1000872]">
<correction r-cran-igraph "Исправление отказа в обслуживании из-за специально сформированного объекта [CVE-2018-20349]">
<correction rails "Исправление раскрытия информации [CVE-2018-16476 CVE-2019-5418], отказа в обслуживании [CVE-2019-5419]">
<correction rsync "Несколько исправлений безопасности для zlib [CVE-2016-9840 CVE-2016-9841 CVE-2016-9842 CVE-2016-9843]">
<correction ruby-i18n "Предотвращение удалённого отказа в обслуживании [CVE-2014-10077]">
<correction ruby2.3 "Исправление ошибки сборки из исходного кода">
<correction runc "Исправление повышения привилегий до уровня суперпользователя [CVE-2019-5736]">
<correction systemd "journald: исправление ошибки утверждения в journal_file_link_data; tmpfiles: исправление <q>e</q> для поддержки шаблонов в стиле командной оболочки; mount-util: принятие того, что name_to_handle_at() может завершиться неудачно с EPERM; automount: подтверждать запросы на автоматическое монтирование, даже если устройство уже примонтировано [CVE-2018-1049]; исправление потенциального повышения привилегий до уровня суперпользователя [CVE-2018-15686]">
<correction twitter-bootstrap3 "Исправление межсайтового скриптинга во всплывающих подсказках и всплывающих меню [CVE-2019-8331]">
<correction tzdata "Новый выпуск основной ветки разработки">
<correction unzip "Исправление переполнения буфера при обработке ZIP-архивов, защищённых паролем [CVE-2018-1000035]">
<correction vcftools "Исправление раскрытия информации [CVE-2018-11099] и отказа в обслуживании [CVE-2018-11129 CVE-2018-11130] из-за специально сформированных файлов">
<correction vips "Исправление разыменования NULL-указателя [CVE-2018-7998], обращения к неинициализированному буферу памяти [CVE-2019-6976]">
<correction waagent "Новый выпуск основной ветки разработки, содержащий множество исправлений, касающихся Azure [CVE-2019-0804]">
<correction yorick-av "Нормализация временных меток фреймов; установка размера буфера VBV для файлов в форматах MPEG1/2">
<correction zziplib "Исправление доступа к неправильному буферу памяти [CVE-2018-6381], ошибки шины [CVE-2018-6540], чтения за пределами выделенного буфера [CVE-2018-7725], аварийной остановки из-за специально сформированного zip-файла [CVE-2018-7726], утечки памяти [CVE-2018-16548]; отклонение ZIP-файла, если размер центрального каталога и/или смещение начала точки центрального каталога находятся за пределами окончания ZIP-файла [CVE-2018-6484, CVE-2018-6541, CVE-2018-6869]">
</table>

<h2>Обновления безопасности</h2>


<p>В данный выпуск внесены следующие обновления безопасности. Команда
безопасности уже выпустила рекомендации для каждого
из этих обновлений:</p>

<table border=0>
<tr><th>Идентификационный номер рекомендации</th>  <th>Пакет</th></tr>
<dsa 2018 4259 ruby2.3>
<dsa 2018 4332 ruby2.3>
<dsa 2018 4341 mariadb-10.1>
<dsa 2019 4373 coturn>
<dsa 2019 4374 qtbase-opensource-src>
<dsa 2019 4377 rssh>
<dsa 2019 4385 dovecot>
<dsa 2019 4387 openssh>
<dsa 2019 4388 mosquitto>
<dsa 2019 4389 libu2f-host>
<dsa 2019 4390 flatpak>
<dsa 2019 4391 firefox-esr>
<dsa 2019 4392 thunderbird>
<dsa 2019 4393 systemd>
<dsa 2019 4394 rdesktop>
<dsa 2019 4396 ansible>
<dsa 2019 4397 ldb>
<dsa 2019 4398 php7.0>
<dsa 2019 4399 ikiwiki>
<dsa 2019 4400 openssl1.0>
<dsa 2019 4401 wordpress>
<dsa 2019 4402 mumble>
<dsa 2019 4403 php7.0>
<dsa 2019 4405 openjpeg2>
<dsa 2019 4406 waagent>
<dsa 2019 4407 xmltooling>
<dsa 2019 4408 liblivemedia>
<dsa 2019 4409 neutron>
<dsa 2019 4410 openjdk-8>
<dsa 2019 4411 firefox-esr>
<dsa 2019 4412 drupal7>
<dsa 2019 4413 ntfs-3g>
<dsa 2019 4414 libapache2-mod-auth-mellon>
<dsa 2019 4415 passenger>
<dsa 2019 4416 wireshark>
<dsa 2019 4417 firefox-esr>
<dsa 2019 4418 dovecot>
<dsa 2019 4419 twig>
<dsa 2019 4420 thunderbird>
<dsa 2019 4422 apache2>
<dsa 2019 4423 putty>
<dsa 2019 4424 pdns>
<dsa 2019 4425 wget>
<dsa 2019 4426 tryton-server>
<dsa 2019 4427 samba>
<dsa 2019 4428 systemd>
<dsa 2019 4429 spip>
<dsa 2019 4430 wpa>
<dsa 2019 4431 libssh2>
<dsa 2019 4432 ghostscript>
<dsa 2019 4433 ruby2.3>
<dsa 2019 4434 drupal7>
</table>


<h2>Удалённые пакеты</h2>

<p>Следующие пакеты были удалены из-за обстоятельств, на которые мы не
можем повлиять:</p>


<table border=0>
<tr><th>Пакет</th>               <th>Причина</th></tr>
<correction gcontactsync "Несовместим с новыми версиями firefox-esr">
<correction google-tasks-sync "Несовместим с новыми версиями firefox-esr">
<correction mozilla-gnome-kerying "Несовместим с новыми версиями firefox-esr">
<correction tbdialout "Несовместим с новыми версиями thunderbird">
<correction timeline "Несовместим с новыми версиями thunderbird">

</table>

<h2>Программа установки Debian</h2>

Программа установки была обновлена с целью включения исправлений, добавленных в
данную редакцию стабильного выпуска.

<h2>URL</h2>

<p>Полный список пакетов, которые были изменены в данной редакции:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Текущий стабильный выпуск:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>Предлагаемые обновления для стабильного выпуска:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>Информация о стабильном выпуске (информация о выпуске, известные ошибки и т. д.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Анонсы безопасности и информация:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>О Debian</h2>

<p>Проект Debian &mdash; объединение разработчиков свободного программного обеспечения,
которые жертвуют своё время и знания для создания абсолютно свободной
операционной системы Debian.</p>


<h2>Контактная информация</h2>

<p>Более подробную информацию вы можете получить на сайте Debian
<a href="$(HOME)/">https://www.debian.org/</a>, либо отправив письмо по адресу
&lt;press@debian.org&gt;, либо связавшись с командой стабильного выпуска по адресу
&lt;debian-release@lists.debian.org&gt;.</p>
