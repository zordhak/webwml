# Brazilian Portuguese translation for Debian website vote.pot
# Copyright (C) 2002-2015 Software in the Public Interest, Inc.
#
# Philipe Gaspar <philipegaspar@terra.com.br>, 2002
# Michelle Ribeiro <michelle@cipsga.org.br>, 2003
# Gustavo R. Montesino <grmontesino@ig.com.br>, 2004
# Felipe Augusto van de Wiel (faw) <faw@debian.org>, 2006-2008.
# Marcelo Gomes de Santana <marcelo@msantana.eng.br>, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: Debian Webwml\n"
"PO-Revision-Date: 2015-07-04 12:01-0300\n"
"Last-Translator: Marcelo Gomes de Santana <marcelo@msantana.eng.br>\n"
"Language-Team: Brazilian Portuguese <debian-l10n-portuguese@lists.debian."
"org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/template/debian/votebar.wml:13
msgid "Date"
msgstr "Data"

#: ../../english/template/debian/votebar.wml:16
msgid "Time Line"
msgstr "Linha do Tempo"

#: ../../english/template/debian/votebar.wml:19
msgid "Summary"
msgstr "Resumo"

#: ../../english/template/debian/votebar.wml:22
msgid "Nominations"
msgstr "Candidaturas"

#: ../../english/template/debian/votebar.wml:25
#, fuzzy
msgid "Withdrawals"
msgstr "Retirado"

#: ../../english/template/debian/votebar.wml:28
msgid "Debate"
msgstr "Debate"

#: ../../english/template/debian/votebar.wml:31
msgid "Platforms"
msgstr "Plataformas"

#: ../../english/template/debian/votebar.wml:34
msgid "Proposer"
msgstr "Proponente"

#: ../../english/template/debian/votebar.wml:37
msgid "Proposal A Proposer"
msgstr "Proponente da Proposta A"

#: ../../english/template/debian/votebar.wml:40
msgid "Proposal B Proposer"
msgstr "Proponente da Proposta B"

#: ../../english/template/debian/votebar.wml:43
msgid "Proposal C Proposer"
msgstr "Proponente da Proposta C"

#: ../../english/template/debian/votebar.wml:46
msgid "Proposal D Proposer"
msgstr "Proponente da Proposta D"

#: ../../english/template/debian/votebar.wml:49
msgid "Proposal E Proposer"
msgstr "Proponente da Proposta E"

#: ../../english/template/debian/votebar.wml:52
msgid "Proposal F Proposer"
msgstr "Proponente da Proposta F"

#: ../../english/template/debian/votebar.wml:55
#, fuzzy
msgid "Proposal G Proposer"
msgstr "Proponente da Proposta A"

#: ../../english/template/debian/votebar.wml:58
#, fuzzy
msgid "Proposal H Proposer"
msgstr "Proponente da Proposta A"

#: ../../english/template/debian/votebar.wml:61
msgid "Seconds"
msgstr "Padrinhos"

#: ../../english/template/debian/votebar.wml:64
msgid "Proposal A Seconds"
msgstr "Padrinhos da Proposta A"

#: ../../english/template/debian/votebar.wml:67
msgid "Proposal B Seconds"
msgstr "Padrinhos da Proposta B"

#: ../../english/template/debian/votebar.wml:70
msgid "Proposal C Seconds"
msgstr "Padrinhos da Proposta C"

#: ../../english/template/debian/votebar.wml:73
msgid "Proposal D Seconds"
msgstr "Padrinhos da Proposta D"

#: ../../english/template/debian/votebar.wml:76
msgid "Proposal E Seconds"
msgstr "Padrinhos da Proposta E"

#: ../../english/template/debian/votebar.wml:79
msgid "Proposal F Seconds"
msgstr "Padrinhos da Proposta F"

#: ../../english/template/debian/votebar.wml:82
#, fuzzy
msgid "Proposal G Seconds"
msgstr "Padrinhos da Proposta A"

#: ../../english/template/debian/votebar.wml:85
#, fuzzy
msgid "Proposal H Seconds"
msgstr "Padrinhos da Proposta A"

#: ../../english/template/debian/votebar.wml:88
msgid "Opposition"
msgstr "Oposição"

#: ../../english/template/debian/votebar.wml:91
msgid "Text"
msgstr "Texto"

#: ../../english/template/debian/votebar.wml:94
msgid "Proposal A"
msgstr "Proposta A"

#: ../../english/template/debian/votebar.wml:97
msgid "Proposal B"
msgstr "Proposta B"

#: ../../english/template/debian/votebar.wml:100
msgid "Proposal C"
msgstr "Proposta C"

#: ../../english/template/debian/votebar.wml:103
msgid "Proposal D"
msgstr "Proposta D"

#: ../../english/template/debian/votebar.wml:106
msgid "Proposal E"
msgstr "Proposta E"

#: ../../english/template/debian/votebar.wml:109
msgid "Proposal F"
msgstr "Proposta F"

#: ../../english/template/debian/votebar.wml:112
#, fuzzy
msgid "Proposal G"
msgstr "Proposta A"

#: ../../english/template/debian/votebar.wml:115
#, fuzzy
msgid "Proposal H"
msgstr "Proposta A"

#: ../../english/template/debian/votebar.wml:118
msgid "Choices"
msgstr "Opções"

#: ../../english/template/debian/votebar.wml:121
msgid "Amendment Proposer"
msgstr "Proponente da Emenda"

#: ../../english/template/debian/votebar.wml:124
msgid "Amendment Seconds"
msgstr "Padrinhos da Emenda"

#: ../../english/template/debian/votebar.wml:127
msgid "Amendment Text"
msgstr "Texto da Emenda"

#: ../../english/template/debian/votebar.wml:130
msgid "Amendment Proposer A"
msgstr "Proponente da Emenda A"

#: ../../english/template/debian/votebar.wml:133
msgid "Amendment Seconds A"
msgstr "Padrinhos da Emenda A"

#: ../../english/template/debian/votebar.wml:136
msgid "Amendment Text A"
msgstr "Texto da Emenda A"

#: ../../english/template/debian/votebar.wml:139
msgid "Amendment Proposer B"
msgstr "Proponente da Emenda B"

#: ../../english/template/debian/votebar.wml:142
msgid "Amendment Seconds B"
msgstr "Padrinhos da Emenda B"

#: ../../english/template/debian/votebar.wml:145
msgid "Amendment Text B"
msgstr "Texto da Emenda B"

#: ../../english/template/debian/votebar.wml:148
msgid "Amendment Proposer C"
msgstr "Proponente da Emenda C"

#: ../../english/template/debian/votebar.wml:151
msgid "Amendment Seconds C"
msgstr "Padrinhos da Emenda C"

#: ../../english/template/debian/votebar.wml:154
msgid "Amendment Text C"
msgstr "Texto da Emenda C"

#: ../../english/template/debian/votebar.wml:157
msgid "Amendments"
msgstr "Emendas"

#: ../../english/template/debian/votebar.wml:160
msgid "Proceedings"
msgstr "Encaminhamentos"

#: ../../english/template/debian/votebar.wml:163
msgid "Majority Requirement"
msgstr "Maioria Requerida"

#: ../../english/template/debian/votebar.wml:166
msgid "Data and Statistics"
msgstr "Dados e Estatísticas"

#: ../../english/template/debian/votebar.wml:169
msgid "Quorum"
msgstr "Quorum"

#: ../../english/template/debian/votebar.wml:172
msgid "Minimum Discussion"
msgstr "Discussão Mínima"

#: ../../english/template/debian/votebar.wml:175
msgid "Ballot"
msgstr "Cédula"

#: ../../english/template/debian/votebar.wml:178
msgid "Forum"
msgstr "Fórum"

#: ../../english/template/debian/votebar.wml:181
msgid "Outcome"
msgstr "Resultado"

#: ../../english/template/debian/votebar.wml:185
msgid "Waiting&nbsp;for&nbsp;Sponsors"
msgstr "Aguardando&nbsp;Patrocinadores"

#: ../../english/template/debian/votebar.wml:188
msgid "In&nbsp;Discussion"
msgstr "Em&nbsp;Discussão"

#: ../../english/template/debian/votebar.wml:191
msgid "Voting&nbsp;Open"
msgstr "Votação&nbsp;Aberta"

#: ../../english/template/debian/votebar.wml:194
msgid "Decided"
msgstr "Decidido"

#: ../../english/template/debian/votebar.wml:197
msgid "Withdrawn"
msgstr "Retirado"

#: ../../english/template/debian/votebar.wml:200
msgid "Other"
msgstr "Outro"

#: ../../english/template/debian/votebar.wml:204
msgid "Home&nbsp;Vote&nbsp;Page"
msgstr "Página&nbsp;principal&nbsp;de&nbsp;Votação"

#: ../../english/template/debian/votebar.wml:207
msgid "How&nbsp;To"
msgstr "Como Fazer"

#: ../../english/template/debian/votebar.wml:210
msgid "Submit&nbsp;a&nbsp;Proposal"
msgstr "Enviar&nbsp;Proposta"

#: ../../english/template/debian/votebar.wml:213
msgid "Amend&nbsp;a&nbsp;Proposal"
msgstr "Corrigir&nbsp;uma&nbsp;Proposta"

#: ../../english/template/debian/votebar.wml:216
msgid "Follow&nbsp;a&nbsp;Proposal"
msgstr "Apoiar&nbsp;uma&nbsp;Proposta"

#: ../../english/template/debian/votebar.wml:219
msgid "Read&nbsp;a&nbsp;Result"
msgstr "Ler&nbsp;um&nbsp;Resultado"

#: ../../english/template/debian/votebar.wml:222
msgid "Vote"
msgstr "Votar"
